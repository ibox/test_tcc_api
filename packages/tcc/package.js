/**
 * Created by ibox on 9/17/14.
 */
Package.describe({
    summary: "The ibox Currency Cloud API package"
});

Package.on_use(function (api) {
    api.add_files('tcc.js', 'server');
    if(api.export)
        api.export('Tcc');
});